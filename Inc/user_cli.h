#ifndef USER_CLI_H
#define USER_CLI_H

/** @brief user_CLI: Quick and easy implementation of terminal interface for MCU's

		With user_CLI one can register commands with a callback included. Once the command is registered, the arguments can be added.
		@author M. vd Heuvel, MHElektro
		@date March 2020
		*/

#include <stdint.h>
#include <stdlib.h>

#define CLI_SUPPORT_ARGUMENTS
#define CLI_HELP_ENABLE

#define CLI_HELP_HEADER				"-- start of help --"
#define CLI_HELP_FOOTER				"-- end of help --"
#define CLI_NO_CMD_MSG				"No commands. Add using CLI_register_cmd"

typedef uint8_t * (*cmd_action)(void *);
typedef void (*fcn)(uint8_t *, uint16_t);


typedef struct CLI_st_argument{
	uint8_t * type;									/**< Data type for this argument, NULL is not set yet */
	uint8_t mandatory;							/**< 0 == optional, 1 == mandatory */
	void * data;										/**< The data for users to fetch */
	struct CLI_st_argument * next;	/**< Next argument for more arguments */
}CLI_st_argument;		
		
typedef struct CLI_st_cmd{		
	#ifdef CLI_SUPPORT_ARGUMENTS
	uint8_t nr_of_arguments;				/**< Nr of arguments for this command */
	CLI_st_argument * arguments;		/**< Linked list of the arguments for this command */
	#endif
	#ifdef CLI_HELP_ENABLE
	uint8_t * human_name;						/**< Human readable name is stored */
	#endif
	uint32_t ID;										/**< ID of this command */
	cmd_action action;							/**< Action pointer */
	struct CLI_st_cmd * next;				/**< Next command in list to check */
}CLI_st_cmd;

enum CLI_error{
	CLI_fail,
	CLI_succes
};

void CLI_register_output_callback(fcn callback);
uint8_t CLI_register_cmd(uint8_t * caller, cmd_action action, uint8_t nr_of_arguments);
uint8_t CLI_add_argument(uint8_t * caller, uint8_t * dataype, uint8_t mandatory);
uint8_t CLI_low_level_input(uint8_t * input);
uint8_t CLI_print_help(void);
uint8_t CLI_custom_input(uint8_t * input);

#endif
