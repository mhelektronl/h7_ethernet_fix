//==== MHELEKTRO 2020 ====
//Command Line Interface 

/** @brief user_CLI: Quick and easy implementation of terminal interface for MCU's

		With user_CLI one can register commands with a callback included. Once the command is registered, the arguments can be added.
		@author M. vd Heuvel, MHElektro
		@date March 2020
		*/

#include "user_cli.h"
#include <string.h>



CLI_st_cmd* CLI_cmdlist;
fcn send_output;
/** The CLI_create_ID function creates the hash for quick finding in the commandlist 
*		@param str This is the humand readable name which is hashed
*		@return hash The 4 byte (32 bit) hash 
*/
static uint32_t CLI_create_ID(uint8_t * str){
	uint32_t hash = 5381;
	int c;

	while ((c = *str++))
			hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

	return hash;
}

/**	The split_cmd function splits the command from the arguments
*		@param input The whole input string including arguments
		@return retval Returns a string holding the single command
*/
static uint8_t * split_cmd(uint8_t * input){
	uint8_t * retval;
	for(uint8_t i = 0; i < strlen((char*)input); i++){
		if(input[i] == 0x20){
			//Found first space, change to 0 to terminate and isolate cmd
			retval = malloc(i+1);
			if(!retval)
				return (uint8_t *)"Error with malloc";
			memcpy(retval, input, i);
			return retval;
		}
	}

	//Could be command without arguments
	
	return input;
}

/**	The count_args counts the number of arguments and returns this
*		@param input The whole input string including arguments
		@return retval Returns the number of arguments in the input
*/
static uint8_t count_args(uint8_t * input){
	//Arguments are split using whitespace
	uint16_t end = 0, start = 0;
	uint8_t retval = 0;
	//Remove trailing spaces
	for(int i = strlen((char *)input) - 1; i >= 0; i--){
		//Start from the back 
		if(input[i] > 0x20){
			//Everything other than characters must be ingored (newlines, whitespaces, carriage return etc)
			end = i;
			break;
		}
	}
	
	if(end == 0)
		return retval;
	
	//Single command
	if(end == (strlen((char *)input)-1))
		return retval;
	
	//Count spaces from start to end
	for(int i = start; i <= end; i++){
		if(input[i] == 0x20){
			retval++;
		}
	}

	return retval;
}

/** CLI Reponse function will send the response to the registered output function
*		@param response The response to send 
*		@return CLI_succes
*/
static uint8_t CLI_response(uint8_t * response){
	send_output(response, strlen((char *)response));
	return CLI_succes;
}

#ifdef CLI_HELP_ENABLE
/** CLI_print_help will generate and print a list of commands and their registered arguments
*		@param none
*		@return CLI_succes on succes, CLI_fail when malloc failed or no commands are registered
*/
uint8_t CLI_print_help(void){
	//Print multiple lines in the follow format
	/*
	*		-- start of help --
	*		command01 uint8_t uint16_t string
	*		command02
	*		command03	string
	*		-- end of help --
	*/
	
	//Print header
	CLI_response((uint8_t*)CLI_HELP_HEADER);
	CLI_response((uint8_t*)"\r\n");
	
	//Loop trough command list
	CLI_st_cmd * cmd;
	CLI_st_argument * arg;
	
	cmd = CLI_cmdlist;
	
	//Check if any commands exists
	if(cmd == NULL){
		CLI_response((uint8_t *)CLI_NO_CMD_MSG);
		CLI_response((uint8_t*)"\r\n");
		return CLI_fail;
	}
	
	while(cmd != NULL){
		CLI_response(cmd->human_name);
		//Check for arguments and print them
		if(cmd->nr_of_arguments > 0){
			//Add arguments
			if(cmd->arguments != NULL){
				arg = cmd->arguments;
				while(arg != NULL){
					CLI_response((uint8_t *)" ");
					CLI_response(arg->type);
					arg = arg->next;
				}
			}
		}
		cmd = cmd->next;
	}
	CLI_response((uint8_t*)"\r\n");
	
	//Print footer
	CLI_response((uint8_t*)CLI_HELP_FOOTER);
	CLI_response((uint8_t*)"\r\n");
	return CLI_succes;
}
#endif

/** CLI_custom_input can be used to send text to the console without returning the callback function
*		@param input The input string to be send
*		@return CLI_succes
*/
uint8_t CLI_custom_input(uint8_t * input){
	return CLI_response(input);
}

/** CLI_low_level_input must be fed with terminated lines (no single chars!). This will do the parsing of the command and call callbacks.
*		@param input The input string to be send
*		@return CLI_succes on succes, CLI_fail on error
*/
uint8_t CLI_low_level_input(uint8_t * input){
	uint8_t * command_input;
	//Check input for errors
	if(!input)
		return CLI_response((uint8_t*)"Invalid input\r\n");
	
	//Check if input starts with character
	if(input[0] < 65 || input[0] > 122)
		return CLI_response((uint8_t*)"Invalid input\r\n");
	
	//Remove newlines
	for(int i = strlen((char *)input) - 1; i >= 0; i--){
		//Start from the back 
		if(input[i] > 0x20){
			//Everything other than characters must be ingored (newlines, whitespaces, carriage return etc)
			command_input = malloc(i);
			if(!command_input)
				return CLI_fail;
			memcpy(command_input, input, i+1);
			command_input[i+1] = 0;
			break;
		}
	}
	
	//Check if command exists
	uint32_t hash = CLI_create_ID(split_cmd(command_input));
	CLI_st_cmd *cmd = CLI_cmdlist;
	while(cmd != NULL){
		if(cmd->ID == hash){
			//Found cmd
			break;
		}
		cmd = cmd->next;
	}
	if(!cmd)
		return CLI_response((uint8_t*)"Unkown command\r\n");
	
	//Check for right number of arguments (only if arguments are expected)
	if(cmd->nr_of_arguments > 0 && cmd->nr_of_arguments != count_args(input))
		return CLI_response((uint8_t*)"Not enough arguments\r\n");
	
	
	//Call action handler
	CLI_response(cmd->action(cmd->arguments));
	
	return CLI_succes;
}

/**
*
*/
void CLI_register_output_callback(fcn callback){
	send_output = callback;
}

#ifdef CLI_SUPPORT_ARGUMENTS
/** CLI_add_argument can be invoked after registering commands. This will add arguments to this command, which will me mandatory or optional and having their own type.
*		@param caller The command which this argument applies to
*		@param datatype The type of data of this argument. In string format so any format is accepted 
*		@param mandatory If 1 this argument is mandatory. If 0 this argument is optional
*		@retval CLI_succes
*/
uint8_t CLI_add_argument(uint8_t * caller, uint8_t * dataype, uint8_t mandatory){
	CLI_st_cmd * previous_link;
	CLI_st_argument * previous_arg;
	//Create hash to search for
	uint32_t hash = CLI_create_ID(caller);
	
	//Search right command from list
	
	previous_link = CLI_cmdlist;
	while(previous_link != NULL){
		if(previous_link->ID == hash)
			break;
		previous_link = previous_link->next;
	}
	if(!previous_link)
		return CLI_response((uint8_t*)"Error adding argument: cmd not found\r\n");
	
	//Found right cmd, find last free argument
	previous_arg = previous_link->arguments;
	while(previous_arg != NULL){
		//loop trough possible arguments
		if(previous_arg->type == NULL){
			//Free argument
			break;
		}
		previous_arg = previous_arg->next;
	}
	if(!previous_arg)
		return CLI_response((uint8_t*)"Error adding argument: No room for arguments left\r\n");;	//No room for arguments left
	
	previous_arg->mandatory = mandatory;
	previous_arg->type = dataype;
	
	return CLI_succes;
}
#endif

/** CLI_register_cmd is used to register new commands to the CLI
*		@param caller Caller is a human readable name for the command
*		@param action A function in the form of uint8_t * command_function(void *) which is called when this command is entered
*		@param nr_of_arguments The space for arguments is already malloc'd here, this is the amount of arguments that are needed
*		@return CLI_succes on succes, CLI_fail on error
*/
uint8_t CLI_register_cmd(uint8_t * caller, cmd_action action, uint8_t nr_of_arguments){
	CLI_st_cmd * new_link;
	
	//Check if list is empty (first cmd)
	if(!CLI_cmdlist){
		
		//Create first cmd
		CLI_cmdlist = malloc(sizeof(CLI_st_cmd));
		if(!CLI_cmdlist)
			return CLI_fail;
		new_link = CLI_cmdlist;
	} else {
		//Add command to list
		new_link = malloc(sizeof(CLI_st_cmd));
		if(!new_link)
			return CLI_fail;
		
		//Add this link to list
		CLI_st_cmd * previous_link;
		previous_link = CLI_cmdlist;
		while(previous_link->next != NULL){
			previous_link = previous_link->next;
		}
		previous_link->next = new_link;
	}
	
	//Fill the new link with data	//TODO: Check if ID is non-existent yet
	new_link->human_name = malloc(strlen((char*)caller)+1);
	if(new_link->human_name == NULL){
		CLI_response((uint8_t *)"Unable to store human name for command ");
		CLI_response(caller);
		CLI_response((uint8_t *)"\r\n");
	}
	memcpy(new_link->human_name, caller, strlen((char*)caller)+1);
	new_link->ID = CLI_create_ID(caller);
	new_link->action = action;
	#ifdef CLI_SUPPORT_ARGUMENTS
	new_link->nr_of_arguments = nr_of_arguments;
	if(nr_of_arguments == 0){
		new_link->arguments = NULL;
	} else {
		//Create list for arguments
		CLI_st_argument * arguments, *prev_arg;
		uint8_t current_arg = 0;
		do{
			arguments = malloc(sizeof(CLI_st_argument));
			arguments->next = NULL;
			if(!arguments)
				return CLI_fail;
			if(!new_link->arguments){
				new_link->arguments = arguments;
			} else {
				prev_arg->next = arguments;
			}
			prev_arg = arguments;
			current_arg++;
		}while (current_arg < nr_of_arguments);
	}
	#endif
	//Set next to NULL because this is the newest link
	new_link->next = NULL;
	
	return CLI_succes;
}
