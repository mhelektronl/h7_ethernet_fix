# README #

Ethernet on STM32H743 isn't working as expected for many people. 
This repo will be used to find the problem, and eventually a fix for this problem.

### What is this repository for? ###

* Ethernet HAL for the STM32H7x3 series
* HAL Version V1.7.0
* CubeMX Version 5.6.0
* Compiled with MDK-ARM V5
* Tested on NUCLEO-H743ZI Rev B
* LwIP version 2.0.3 with RAW API 

Please see www.stmh7.com for more info